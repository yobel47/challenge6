import {StyleSheet} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {Home, Scanner} from '../../pages';
import React from 'react';
import {primaryColor} from '../../utils/colors';
import {Button} from 'react-native-paper';
import auth from '@react-native-firebase/auth';
import {NavigationContainer} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Tab = createBottomTabNavigator();

const BottomNavigator = ({navigation}) => {
  const storeData = async value => {
    try {
      await AsyncStorage.setItem('@user', value);
    } catch (e) {
      // saving error
    }
  };

  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        tabBarIcon: ({color, size}) => {
          let iconName;
          if (route.name === 'Home') {
            iconName = 'home';
          } else if (route.name === 'Scanner') {
            iconName = 'qrcode';
          }
          return <AntDesign name={iconName} size={size} color={color} />;
        },
        tabBarActiveTintColor: primaryColor,
        tabBarStyle: {
          paddingTop: 10,
          height: 60,
        },
        tabBarLabelStyle: {
          marginBottom: 10,
        },
      })}>
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          headerRight: () => (
            <Button
              mode="contained"
              style={{marginRight: 15}}
              onPress={() => {
                auth()
                  .signOut()
                  .then(() => {
                    console.log('User signed out!');
                    navigation.navigate('Login');
                    storeData('logout');
                  })
                  .catch(error => console.log(error));
              }}>
              Sign Out
            </Button>
          ),
        }}
      />
      <Tab.Screen
        name="Scanner"
        component={Scanner}
        options={{
          headerRight: () => (
            <Button
              testID="finger_button"
              mode="contained"
              style={{marginRight: 15}}
              onPress={() => {
                auth()
                  .signOut()
                  .then(() => {
                    console.log('User signed out!');
                    navigation.navigate('Login');
                    storeData('logout');
                  });
              }}>
              Sign Out
            </Button>
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default BottomNavigator;

const styles = StyleSheet.create({});
