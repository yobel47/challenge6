import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {Login} from '../pages';
import BottomNavigator from '../components/BottomNavigator';

const Stack = createStackNavigator();

const Router = () => {
  return (
    <Stack.Navigator initialRouteName="Login">
      <Stack.Screen
        name="Login"
        component={Login}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="MainApp"
        component={BottomNavigator}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

export default Router;
