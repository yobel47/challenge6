import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  PermissionsAndroid,
  Platform,
  ToastAndroid,
  Alert,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import MapView, {Marker, PROVIDER_GOOGLE, Callout} from 'react-native-maps';
import Geolocation from 'react-native-geolocation-service';
import analytics from '@react-native-firebase/analytics';

const Home = () => {
  const [location, setLocation] = useState(null);

  useEffect(() => {
    onLogScreenView();
    getLocation();
  }, []);

  async function onLogScreenView() {
    try {
      await analytics().logScreenView({
        screen_name: 'Home',
        screen_class: 'Home',
      });
    } catch (error) {
      console.log(error);
    }
  }

  const getLocation = async () => {
    const hasPermission = await hasLocationPermission();

    if (!hasPermission) {
      return;
    }

    Geolocation.getCurrentPosition(
      position => {
        const {latitude, longitude} = position.coords;
        setLocation({
          ...location,
          latitude,
          longitude,
        });
      },
      error => {
        Alert.alert(`Code ${error.code}`, error.message);
        setLocation(null);
        console.log(error);
      },
      {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000},
    );
  };

  const hasLocationPermission = async () => {
    if (Platform.OS === 'ios') {
      const hasPermission = await hasPermissionIOS();
      return hasPermission;
    }

    if (Platform.OS === 'android' && Platform.Version < 23) {
      return true;
    }

    const hasPermission = await PermissionsAndroid.check(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    );

    if (hasPermission) {
      return true;
    }

    const status = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    );

    if (status === PermissionsAndroid.RESULTS.GRANTED) {
      return true;
    }

    if (status === PermissionsAndroid.RESULTS.DENIED) {
      ToastAndroid.show(
        'Location permission denied by user.',
        ToastAndroid.LONG,
      );
    } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
      ToastAndroid.show(
        'Location permission revoked by user.',
        ToastAndroid.LONG,
      );
    }

    return false;
  };

  return (
    <View style={styles.container}>
      {location && (
        <MapView
          style={styles.map}
          provider={PROVIDER_GOOGLE}
          initialRegion={{
            latitude: location.latitude,
            longitude: location.longitude,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          }}
          showsUserLocation={true}
          paddingAdjustmentBehavior="automatic"
          showsMyLocationButton={true}
          showsBuildings={true}
          maxZoomLevel={17.5}
          loadingEnabled={true}
          loadingIndicatorColor="#fcb103"
          loadingBackgroundColor="#242f3e">
          <Marker
            coordinate={{
              latitude: -7.457535439105736,
              longitude: 112.44573726443826,
            }}>
            <Callout tooltip>
              <View
                style={{
                  flex: 1,
                  backgroundColor: 'rgba(255,255,255,1)',
                  paddingHorizontal: 18,
                  paddingVertical: 12,
                  borderRadius: 20,
                }}>
                <Text>SDN Wates III</Text>
              </View>
            </Callout>
          </Marker>
          <Marker
            coordinate={{
              latitude: -7.4622252968335925,
              longitude: 112.43304785791376,
            }}>
            <Callout tooltip>
              <View
                style={{
                  flex: 1,
                  backgroundColor: 'rgba(255,255,255,1)',
                  paddingHorizontal: 18,
                  paddingVertical: 12,
                  borderRadius: 20,
                }}>
                <Text>Alun-alun Mojokerto</Text>
              </View>
            </Callout>
          </Marker>
          <Marker
            coordinate={{
              latitude: -7.471420307663597,
              longitude: 112.44493265584683,
            }}>
            <Callout tooltip>
              <View
                style={{
                  flex: 1,
                  backgroundColor: 'rgba(255,255,255,1)',
                  paddingHorizontal: 18,
                  paddingVertical: 12,
                  borderRadius: 20,
                }}>
                <Text>Sunrise Mall</Text>
              </View>
            </Callout>
          </Marker>
        </MapView>
      )}
    </View>
  );
};

export default Home;

const window = Dimensions.get('window');
const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    height: window.height - 140,
    width: window.width,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});
