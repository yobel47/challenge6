import {View, Text, ScrollView, StatusBar, Dimensions} from 'react-native';
import React, {useState, useEffect} from 'react';
import {
  TextInput,
  Button,
  HelperText,
  Dialog,
  Portal,
} from 'react-native-paper';
import styles from './styles';
import {primaryColor} from '../../utils/colors';
import {LoginImage} from '../../assets';
import auth from '@react-native-firebase/auth';
import {
  GoogleSigninButton,
  GoogleSignin,
} from '@react-native-google-signin/google-signin';
import TouchID from 'react-native-touch-id';
import crashlytics from '@react-native-firebase/crashlytics';
import analytics from '@react-native-firebase/analytics';
import AsyncStorage from '@react-native-async-storage/async-storage';

const window = Dimensions.get('screen');

const Login = ({navigation}) => {
  const [passwordVisible, setPasswordVisible] = useState(true);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [showAlert, setAlert] = useState(false);
  const [alertMessage, setAlertMessage] = useState('');

  useEffect(() => {
    crashlytics().log('App mounted.');
    onLogScreenView();
    GoogleSignin.configure({
      webClientId:
        '89465209908-i5hb5d570jrm0esjtb50iusbf7843ojd.apps.googleusercontent.com',
    });
    getData().then(result => {
      if (result == 'login') {
        pressHandler();
      }
    });
  }, []);

  const storeData = async value => {
    try {
      await AsyncStorage.setItem('@user', value);
    } catch (e) {
      // saving error
    }
  };

  const getData = async () => {
    try {
      const value = await AsyncStorage.getItem('@user');
      if (value !== null) {
        return value;
      }
      return null;
    } catch (e) {
      // error reading value
    }
  };

  const hideDialog = () => setAlert(false);

  async function onLogScreenView() {
    try {
      await analytics().logScreenView({
        screen_name: 'Login',
        screen_class: 'Login',
      });
    } catch (error) {
      console.log(error);
    }
  }

  async function singinEmailPassword() {
    auth()
      .signInWithEmailAndPassword(email, password)
      .then(() => {
        storeData('login');
        navigation.navigate('MainApp');
        console.log('User account created & signed in!');
      })
      .catch(error => {
        if (error.code === 'auth/email-already-in-use') {
          setAlert(true);
          setAlertMessage('That email address is already in use!');
          console.log('That email address is already in use!');
        }

        if (error.code === 'auth/invalid-email') {
          setAlert(true);
          setAlertMessage('That email address is invalid!');
          console.log('That email address is invalid!');
        }

        if (error.code === 'auth/wrong-password') {
          setAlert(true);
          setAlertMessage(
            'The password is invalid or the user does not have a password',
          );
          console.log(
            'The password is invalid or the user does not have a password',
          );
        }

        console.log(error);
      });
  }

  async function onGoogleButtonPress() {
    const {idToken} = await GoogleSignin.signIn();
    const googleCredential = auth.GoogleAuthProvider.credential(idToken);
    storeData('login');
    return auth().signInWithCredential(googleCredential);
  }

  const optionalConfigObject = {
    title: 'Authentication Required',
    color: '#e00606',
    fallbackLabel: 'Show Passcode',
  };

  function pressHandler() {
    TouchID.authenticate('Please authentication', optionalConfigObject)
      .then(success => {
        navigation.navigate('MainApp');
      })
      .catch(error => {
        console.log(error);
      });
  }

  const clickHandler = () => {
    TouchID.isSupported()
      .then(biometryType => {
        if (biometryType === 'FaceID') {
          alert('FaceId support');
        } else if (biometryType === 'TouchID') {
          alert('TouchId support');
        } else if (biometryType === true) {
          alert('supported touchid for android');
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  return (
    <ScrollView
      key="login"
      testID="login_view"
      keyboardShouldPersistTaps="handled"
      style={styles.container}>
      <StatusBar backgroundColor={primaryColor} barStyle="light-content" />

      <View style={styles.containerContent}>
        <LoginImage height={window.width * 0.6} width={window.width} />
        <View style={styles.lineContainer}>
          <View style={styles.line} />
        </View>
        <View style={styles.formContainer}>
          <Text style={styles.headerText}>Login</Text>
          <TextInput
            testID="email_input"
            placeholder="Email"
            mode="outlined"
            label="Email"
            style={styles.textInput}
            value={email}
            onChangeText={text => setEmail(text)}
          />

          <TextInput
            testID="password_input"
            placeholder="Password"
            mode="outlined"
            label="Password"
            style={styles.textInput}
            value={password}
            onChangeText={text => setPassword(text)}
            secureTextEntry={passwordVisible}
            right={
              <TextInput.Icon
                name={passwordVisible ? 'eye' : 'eye-off'}
                onPress={() => setPasswordVisible(!passwordVisible)}
              />
            }
          />

          <Button
            testID="login_button"
            mode="contained"
            style={styles.loginButton}
            onPress={() => {
              singinEmailPassword();
            }}>
            Login
          </Button>

          <Button
            testID="finger_button"
            mode="contained"
            style={styles.loginButton}
            onPress={() => {
              pressHandler();
            }}>
            Fingerprint
          </Button>

          <GoogleSigninButton
            style={{
              marginTop: 12,
              borderRadius: 50,
              width: window.width - 72,
            }}
            size={GoogleSigninButton.Size.Wide}
            color={GoogleSigninButton.Color.Dark}
            onPress={() =>
              onGoogleButtonPress()
                .then(() => navigation.navigate('MainApp'))
                .catch(error => console.log(error))
            }
          />
        </View>
      </View>
      <Portal>
        <Dialog visible={showAlert} dismissable={false}>
          <Dialog.Title style={styles.dialogTitle}>
            <Text>Failed</Text>
          </Dialog.Title>
          <Dialog.Content style={styles.dialogContentContainer}>
            <Text style={styles.dialogContentText}>{alertMessage}!</Text>
            <Button
              mode="contained"
              style={styles.loginButton}
              onPress={() => hideDialog()}>
              Close
            </Button>
          </Dialog.Content>

          <View style={styles.closeAlertContainer}>
            <View style={styles.closeButton}>
              <Button
                icon="close"
                mode="contained"
                style={{
                  marginLeft: 16,
                }}
                onPress={() => hideDialog()}
              />
            </View>
          </View>
        </Dialog>
      </Portal>
    </ScrollView>
  );
};

export default Login;
