import {Linking} from 'react-native';
import React, {useEffect} from 'react';
import {CameraScreen, CameraType} from 'react-native-camera-kit';
import analytics from '@react-native-firebase/analytics';

const Scanner = () => {
  const onReadCode = data => {
    Linking.openURL(data.nativeEvent.codeStringValue);
  };

  useEffect(() => {
    onLogScreenView();
  }, []);

  async function onLogScreenView() {
    try {
      await analytics().logScreenView({
        screen_name: 'Login',
        screen_class: 'Login',
      });
    } catch (error) {
      console.log(error);
    }
  }
  return (
    <CameraScreen
      cameraType={CameraType.Back}
      scanBarcode
      showFrame
      laserColor="white"
      frameColor="white"
      onReadCode={event => {
        onReadCode(event);
      }}
      hideControls
    />
  );
};

export default Scanner;
